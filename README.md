<!--
SPDX-FileCopyrightText: 2023 Christian Meeßen (GFZ) <christian.meessen@gfz-potsdam.de>
SPDX-FileCopyrightText: 2023 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences

SPDX-License-Identifier: CC-BY-4.0
-->

# Documenting the RSD

[Visit the documentation here.](https://research-software-directory.github.io/documentation)

Any changes merge to the main branch will trigger automatically a build deploy to github pages.

## Running locally:
```bash
yarn install
yarn dev
```
## Add a new document to the guide

Edit the information inside the `/docs` folder.

## Creating a release

To create a new release:

- create a tag in master branch
- CI/CD will automatically build the documentation and create a release
- the latest built documentation can be downloaded via [this link](https://codebase.helmholtz.cloud/research-software-directory/RSD-documentation/-/releases/permalink/latest/downloads/RSD-documentation.tar.gz)

## License hint


The content of this repository is licensed under several licenses. We follow the [REUSE specification](https://reuse.software/) to indicate which license applies to the files specifically. Here are some general hints:

- Helmholtz specific source code is licensed under `EUPL-1.2`
- Source code coming from upstream is licensed under `Apache-2.0`
- Documentation and most images are licensed under `CC BY-4.0`
- Some files with trivial content, e.g. configuration files, are licensed under `CC0-1.0`

For more details on the licenses, please have a look at the file headers or associated `*.license` files. The terms of all used licenses are located in the [LICENSES](./LICENSES/) directory.
