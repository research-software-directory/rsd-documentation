<!--
SPDX-FileCopyrightText: 2023 Christian Meeßen (GFZ) <christian.meessen@gfz-potsdam.de>
SPDX-FileCopyrightText: 2023 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences

SPDX-License-Identifier: CC-BY-4.0
-->

# Frequently Asked Questions

## How can I display images in my software/project description?

Currently, the RSD does not support uploading of images in the description. However, you can upload your images elsewhere and reference it using the Markdown image tag:

```markdown
![Description](https://url-to-image/)
```

Images can for example be uploaded to a GitLab instance of your choice (repository must be publicly available), or a pad on [notes.desy.de](https://notes.desy.de).

## How can I integrate videos into my software/project description?

The RSD does not natively support video integration. You can instead post a screenshot of a video and link it to the video hosted on a video hosting platform:

```markdown
[![Image description](https://link-to-video-image)](https://link-to-video)
```

## Why is the Helmholz RSD a fork of the original RSD?

The Helmholtz RSD comes with a number of minor adjustments compared to the [original RSD](https://research-software-directory.org) by the Netherlands eScience Center. The most apparent change is the Helmholtz corporate design which comes with some fixes in the frontend code. The most comfortable way to maintian these changes is to have a fork of the RSD.
