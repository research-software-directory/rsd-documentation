// SPDX-FileCopyrightText: 2022 Jesús García Gonzalez (Netherlands eScience Center) <j.g.gonzalez@esciencecenter.nl>
// SPDX-FileCopyrightText: 2022 Netherlands eScience Center
// SPDX-FileCopyrightText: 2023 Christian Meeßen (GFZ) <christian.meessen@gfz-potsdam.de>
// SPDX-FileCopyrightText: 2023 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
//
// SPDX-License-Identifier: Apache-2.0
// SPDX-License-Identifier: EUPL-1.2

module.exports = {
  // site config
  lang: 'en-US',
  title: 'RSD Documentation',
  description: 'RSD As a Service documentation',
  base: '/documentation/',
  port: '3030',

  themeConfig: {
    logo: '/images/helmholtz_icon.svg',
    logoDark: '/images/helmholtz_icon_inverted.svg',
    repo: 'https://codebase.helmholtz.cloud/research-software-directory/rsd-documentation',
    docsDir: 'docs',
    docsBranch: 'main',
    displayAllHeaders: false, // Default: false

    navbar: [
      {
        text: 'Documentation',
        link: 'introduction'
      },
      {
        text: 'helmholtz.software',
        link: 'https://helmholtz.software'
      },
      {
        text: 'Privacy',
        link: 'https://helmholtz.software/page/privacy-statement'
      },
      {
        text: 'Imprint',
        link: 'https://helmholtz.software/page/imprint'
      }
    ],
    sidebarDepth: 2,

    sidebar: [
      'introduction',
      'navigation',
      'online-demo',
      'getting-access',
      'adding-software',
      'adding-projects',
      'maintain-organisation',
      'faq',
    ],
  },

  plugins: [
    // '@vuepress/plugin-back-to-top'
    '@vuepress/plugin-external-link-icon',
    '@vuepress/plugin-nprogress',
    '@vuepress/plugin-prismjs',
     [ '@vuepress/plugin-palette', { preset: 'sass' } ],
    [
      '@vuepress/plugin-search',
      {
        locales: {
          '/': {
            placeholder: 'Search',
          },
        },
      },
    ],
  ],


}
