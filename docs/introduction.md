<!--
SPDX-FileCopyrightText: 2022 Jason Maassen (Netherlands eScience Center) <j.maassen@esciencecenter.nl>
SPDX-FileCopyrightText: 2022 Jesús García Gonzalez (Netherlands eScience Center) <j.g.gonzalez@esciencecenter.nl>
SPDX-FileCopyrightText: 2022 Netherlands eScience Center
SPDX-FileCopyrightText: 2023 Christian Meeßen (GFZ) <christian.meessen@gfz-potsdam.de>
SPDX-FileCopyrightText: 2023 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences

SPDX-License-Identifier: CC-BY-4.0
-->

# Introduction

Welcome to the user documentation of the [Helmholtz Research Software Directory](https://helmholtz.software)!

Research software is one of the most important scientific instruments available today. Data-intensive research and computational simulation are the foundation of many scientific
discoveries.

The Helmholtz Research Software Directory (RSD) is an online platform designed to showcase the impact of research software that is being developed within the [Helmholtz Association of Research Centres](https://helmholtz.de) on research and society. By showing research software together with
relevant contextual information such as scientific publications, contributors, projects, citation information and much more, we stimulate the reuse and encourage proper citation
to ensure the researchers and RSEs developing the software get credit for their work.

The Helmholtz RSD is a fork of the RSD by the [Netherlands eScience Center](https://esciencecenter.nl). More information about the concepts behind the RSD can be found in this excellent
[blog](https://blog.esciencecenter.nl/the-research-software-directory-and-how-it-promotes-software-citation-4bd2137a6b8).

## About the RSD

The Research Software Directory was initiated in 2017 by the [Netherlands eScience Center](https://esciencecenter.nl) (NLESC) as a platform to showcase the research software developed at
our center. After successfully using the RSD for several years, the NLESC decided in 2021 to re-engineer the platform so we can offer it as an online service to other research
organisations. In 2022, the development effort was joined by [HIFIS](https://hifis.net/).
